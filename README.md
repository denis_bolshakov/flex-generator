# Flex container style generator

This is a test project that allows you to generate user css styles for container using FlexBox.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

just run index.html || npm run build for rebuild

## Running the tests

keep working

## Built With

- [ES6] - native JavaScript
- [webpack](https://webpack.js.org/) - Module bundler.
- [babel](https://babeljs.io/) - JavaScript compiler.

## Authors

- **Denis Bolshakov** - _Initial work_ - (http://basicweb.ru)

## License

This project is licensed under the MIT License
