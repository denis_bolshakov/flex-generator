const container = document.getElementById("flex-container"),
  containerHeightControl = document.getElementById("containerHeightControl"),
  alignContentControl = document.getElementById("alignContentControl"),
  justifyContentControl = document.getElementById("justifyContentControl"),
  alignItemsControl = document.getElementById("alignItemsControl"),
  displayControl = document.getElementById("displayControl"),
  wrapControl = document.getElementById("wrapControl"),
  directionControl = document.getElementById("directionControl"),
  elementsCountControl = document.getElementById("elementsCountControl");

const elementsWidthControl = document.getElementById("elementsWidthControl"),
  elementsHeightControl = document.getElementById("elementsHeightControl"),
  elementsMarginControl = document.getElementById("elementsMarginControl"),
  elementsPaddingControl = document.getElementById("elementsPaddingControl");

const elementWidthControl = document.getElementById("elementWidthControl"),
  elementHeightControl = document.getElementById("elementHeightControl"),
  elementMarginControl = document.getElementById("elementMarginControl"),
  elementPaddingControl = document.getElementById("elementPaddingControl"),
  orderControl = document.getElementById("orderControl"),
  alignSelfControl = document.getElementById("alignSelfControl"),
  growControl = document.getElementById("growControl"),
  shrinkControl = document.getElementById("shrinkControl"),
  basisControl = document.getElementById("basisControl");

let activeElement = false;
let controls = {};

// css for container and elements
const generateCssText = function() {
  const cssCode = document.getElementById("css-code-container");

  let cssText = `<b>*</b> <span class="sko">{</span><span class="att">box-sizing</span>: <span class="zna">border-box</span>; <span class="sko">}</span>\n`;
  cssText += `<b>#flex-container</b> <span class="sko">{</span><span class="att">background</span>: <span class="zna">rgb(0, 150, 208)</span>; `;

  cssText += `<span class="att">display</span>: <span class="zna">${
    displayControl.value
  }</span>; `;

  cssText += `<span class="att">height</span>: <span class="zna">${
    containerHeightControl.value
  }</span>; `;

  if (directionControl.value !== "row") {
    cssText += `<span class="att">flex-direction</span>: <span class="zna">${
      directionControl.value
    }</span>; `;
  }

  cssText += `<span class="att">flex-wrap</span>: <span class="zna">${
    wrapControl.value
  }</span>; `;

  if (alignContentControl.value !== "stretch") {
    cssText += `<span class="att">align-content</span>: <span class="zna">${
      alignContentControl.value
    }</span>; `;
  }

  if (justifyContentControl.value !== "flex-start") {
    cssText += `<span class="att">justify-content</span>: <span class="zna">${
      justifyContentControl.value
    }</span>; `;
  }

  if (alignItemsControl.value !== "stretch") {
    cssText += `<span class="att">align-items</span>: <span class="zna">${
      alignItemsControl.value
    }</span>; `;
  }

  cssText += `<span class="sko">}</span>\n`;

  cssText += `<b>#flex-container > div</b> <span class="sko">{</span><span class="att">background</span>: <span class="zna">rgb(241, 101, 41)</span>; `;
  cssText += `<span class="att">border</span>: <span class="zna">1px solid</span>; `;
  if (elementsWidthControl.value !== "auto") {
    cssText += `<span class="att">width</span>: <span class="zna">${
      elementsWidthControl.value
    }</span>; `;
  }

  if (elementsHeightControl.value !== "auto") {
    cssText += `<span class="att">height</span>: <span class="zna">${
      elementsHeightControl.value
    }</span>; `;
  }

  cssText += `<span class="att">margin</span>: <span class="zna">${
    elementsMarginControl.value
  }</span>; `;
  cssText += `<span class="att">padding</span>: <span class="zna">${
    elementsPaddingControl.value
  }</span>; `;
  cssText += `<span class="sko">}</span>\n`;

  // check for own element props in controls
  const keys = Object.keys(controls);
  if (keys.length !== 0) {
    // sort by number and iterate
    keys.sort().forEach(function(elem) {
      const num = elem.match(/\d+/)[0];
      cssText += `<b>#flex-container > div:nth-of-type(<span class="sko">${num}</span>)</b> <span class="sko">{</span>`;
      for (prop in controls[elem]) {
        // check auto (initial) value props
        if (controls[elem][prop] === "auto") {
          continue;
        }
        switch (prop) {
          case "width": {
            cssText += `<span class="att">width</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }
          case "height": {
            cssText += `<span class="att">height</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }
          case "margin": {
            cssText += `<span class="att">margin</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }
          case "padding": {
            cssText += `<span class="att">padding</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }
          case "order": {
            cssText += `<span class="att">order</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }
          case "alignSelf": {
            cssText += `<span class="att">align-self</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }
          case "flexGrow": {
            cssText += `<span class="att">flex-grow</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }
          case "flexShrink": {
            cssText += `<span class="att">flex-shrink</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }
          case "flexBasis": {
            cssText += `<span class="att">flex-basis</span>: <span class="zna">${
              controls[elem][prop]
            }</span>; `;
            break;
          }

          default:
            break;
        }
      }
      cssText += `<span class="sko">}</span>\n`;
    });
  }

  cssCode.innerHTML = cssText;
};

const updateAlignment = function() {
  container.style.alignContent = alignContentControl.value;
  container.style.justifyContent = justifyContentControl.value;
  container.style.alignItems = alignItemsControl.value;
  container.style.display = displayControl.value;
  container.style.flexWrap = wrapControl.value;
  container.style.flexDirection = directionControl.value;
  container.style.height = containerHeightControl.value;
  // generate CSS visualization
  generateCssText();
};

const containerControls = [
  containerHeightControl,
  alignContentControl,
  justifyContentControl,
  alignItemsControl,
  displayControl,
  wrapControl,
  directionControl
].forEach(elem => elem.addEventListener("change", updateAlignment));

updateAlignment();

// html for container and elements
const generateHtmlText = function() {
  const htmlCode = document.getElementById("html-code-container");
  const blocks = Number(elementsCountControl.value);

  let htmlText =
    "<span class='tag'>&lt;div <span class='att'>id</span>=<span class='zna'>'flex-container'</span>&gt;</span>\n&nbsp;&nbsp;";
  for (let i = 1; i < blocks + 1; i++) {
    htmlText += `<span class="tag">&lt;div&gt;</span>${i}<span class="tag">&lt;/div&gt;</span>`;
  }
  htmlText += `\n<span class='tag'>&lt;/div&gt;</span>`;
  htmlCode.innerHTML = htmlText;
};

const generateElements = function() {
  let currentValue = Number(elementsCountControl.value);

  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }

  for (let i = 1; i < currentValue + 1; i++) {
    let newElem = document.createElement("div");
    newElem.setAttribute("data-index", i);
    newElem.innerHTML = i;

    // check active element
    if (activeElement && activeElement == i) {
      newElem.style.background = "green";
    }

    //check controls for element
    const elem = `data-index-${i}`;
    if (controls.hasOwnProperty(elem)) {
      // all controls css properties
      const properties = Object.keys(controls[elem]);
      if (properties.length > 0) {
        // set value to all properties in object
        properties.forEach(prop => {
          newElem.style[prop] = controls[elem][prop];
        });
      }
    }

    container.appendChild(newElem);
  }

  // check if active deleted
  if (activeElement && activeElement > currentValue) {
    activeElement = false;
    document.getElementsByClassName("info")[0].style.display = "block";
    document.getElementsByClassName(
      "current-element-control"
    )[0].style.display = "none";
  }
  // generate HTML visualization
  generateHtmlText();
  // generate CSS visualization
  generateCssText();
};
generateElements();

const setElementsControl = function(e) {
  const elem = e.target.id;
  const currentValue = e.target.value;
  const containerElements = document.querySelectorAll("#flex-container>div");

  switch (elem) {
    case "elementsCountControl": {
      const elementsBefore = Number(containerElements.length);
      const elementsAfter = Number(currentValue);

      if (elementsAfter >= elementsBefore) {
        generateElements();
      } else {
        for (let i = elementsAfter + 1; i < elementsBefore + 1; i++) {
          const elem = `data-index-${i}`;
          delete controls[elem];
        }
        generateElements();
      }

      break;
    }
    case "elementsWidthControl": {
      containerElements.forEach(e => {
        // check for own props
        const elem = `data-index-${e.getAttribute("data-index")}`;
        if (controls.hasOwnProperty(elem)) {
          if (controls[elem].hasOwnProperty("width")) {
            return;
          }
        }
        e.style.width = currentValue;
      });
      elementWidthControl.value = currentValue;
      break;
    }
    case "elementsHeightControl": {
      containerElements.forEach(e => {
        // check for own props
        const elem = `data-index-${e.getAttribute("data-index")}`;
        if (controls.hasOwnProperty(elem)) {
          if (controls[elem].hasOwnProperty("height")) {
            return;
          }
        }
        e.style.height = currentValue;
      });
      elementHeightControl.value = currentValue;
      break;
    }
    case "elementsMarginControl": {
      containerElements.forEach(e => {
        // check for own props
        const elem = `data-index-${e.getAttribute("data-index")}`;
        if (controls.hasOwnProperty(elem)) {
          if (controls[elem].hasOwnProperty("margin")) {
            return;
          }
        }
        e.style.margin = currentValue;
      });
      elementMarginControl.value = currentValue;
      break;
    }
    case "elementsPaddingControl": {
      containerElements.forEach(e => {
        // check for own props
        const elem = `data-index-${e.getAttribute("data-index")}`;
        if (controls.hasOwnProperty(elem)) {
          if (controls[elem].hasOwnProperty("padding")) {
            return;
          }
        }
        e.style.padding = currentValue;
      });
      elementPaddingControl.value = currentValue;
      break;
    }
    default:
      break;
  }

  generateCssText();
};

const elementsControls = [
  elementsCountControl,
  elementsWidthControl,
  elementsHeightControl,
  elementsMarginControl,
  elementsPaddingControl
].forEach(elem => elem.addEventListener("change", setElementsControl));

const setActiveElement = e => {
  // check if not active element click
  if (Number(e.target.getAttribute("data-index")) !== activeElement) {
    elementWidthControl.value = elementsWidthControl.value;
    elementHeightControl.value = elementsHeightControl.value;
    elementMarginControl.value = elementsMarginControl.value;
    elementPaddingControl.value = elementsPaddingControl.value;
    orderControl.value = 0;
    alignSelfControl.value = "auto";
    growControl.value = 0;
    shrinkControl.value = 1;
    basisControl.value = "auto";

    flexGrowControl.value = 0;
    flexShrinkControl.value = 1;
    flexBasisControl.value = "auto";
  }

  if (Number(e.target.getAttribute("data-index")) === activeElement) {
    activeElement = false;
    document.getElementsByClassName("info")[0].style.display = "block";
    document.getElementsByClassName(
      "current-element-control"
    )[0].style.display = "none";
    e.target.style.background = "rgb(241,101,41)";
    return;
  }

  if (e.target.hasAttribute("data-index")) {
    //remove info message
    document.getElementsByClassName("info")[0].style.display = "none";

    //show current element control
    document.getElementsByClassName(
      "current-element-control"
    )[0].style.display = "block";

    // set active element
    activeElement = Number(e.target.getAttribute("data-index"));
    const containerElements = document.querySelectorAll("#flex-container>div");

    // set initial background-color
    containerElements.forEach(
      elem => (elem.style.background = "rgb(241,101,41)")
    );

    // set background-color for current element
    e.target.style.background = "green";
  }

  // apply controls for element
  const elem = `data-index-${activeElement}`;
  if (controls.hasOwnProperty(elem)) {
    elementWidthControl.value =
      controls[elem].width || elementsWidthControl.value;
    elementHeightControl.value =
      controls[elem].height || elementsHeightControl.value;
    elementMarginControl.value =
      controls[elem].margin || elementsMarginControl.value;
    elementPaddingControl.value =
      controls[elem].padding || elementsPaddingControl.value;
    orderControl.value = controls[elem].order || 0;
    growControl.value = controls[elem].flexGrow || 0;
    shrinkControl.value = controls[elem].flexShrink || 1;
    basisControl.value = controls[elem].flexBasis || "auto";
    alignSelfControl.value = controls[elem].alignSelf || "auto";
    // add controls for flex property
    flexGrowControl.value = growControl.value;
    flexShrinkControl.value = shrinkControl.value;
    flexBasisControl.value = basisControl.value;
  }
};
container.addEventListener("click", setActiveElement);

const setElementControl = function(e) {
  const elemId = e.target.id;
  const currentValue = e.target.value;
  const currentElement = document.querySelector(
    `[data-index='${activeElement}']`
  );
  const elem = `data-index-${activeElement}`;
  const flexGrowControl = document.getElementById("flexGrowControl"),
    flexShrinkControl = document.getElementById("flexShrinkControl"),
    flexBasisControl = document.getElementById("flexBasisControl");

  switch (elemId) {
    case "elementWidthControl": {
      currentElement.style.width = currentValue;

      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          width: currentValue
        }
      };
      break;
    }
    case "elementHeightControl": {
      currentElement.style.height = currentValue;

      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          height: currentValue
        }
      };
      break;
    }
    case "elementMarginControl": {
      currentElement.style.margin = currentValue;

      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          margin: currentValue
        }
      };
      break;
    }
    case "elementPaddingControl": {
      currentElement.style.padding = currentValue;

      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          padding: currentValue
        }
      };
      break;
    }
    case "orderControl": {
      currentElement.style.order = currentValue;

      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          order: currentValue
        }
      };
      break;
    }
    case "alignSelfControl": {
      currentElement.style.alignSelf = currentValue;

      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          alignSelf: currentValue
        }
      };
      break;
    }
    case "growControl": {
      currentElement.style.flexGrow = currentValue;
      flexGrowControl.value = currentValue;
      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          flexGrow: currentValue
        }
      };
      break;
    }
    case "shrinkControl": {
      currentElement.style.flexShrink = currentValue;
      flexShrinkControl.value = currentValue;
      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          flexShrink: currentValue
        }
      };
      break;
    }
    case "basisControl": {
      currentElement.style.flexBasis = currentValue;
      flexBasisControl.value = currentValue;
      controls = {
        ...controls,
        [elem]: {
          ...controls[elem],
          flexBasis: currentValue
        }
      };
      break;
    }
    default:
      break;
  }
  generateCssText();
};

const elementControl = [
  elementWidthControl,
  elementHeightControl,
  elementMarginControl,
  elementPaddingControl,
  orderControl,
  alignSelfControl,
  growControl,
  shrinkControl,
  basisControl
].forEach(elem => elem.addEventListener("change", setElementControl));

const setFlexPropertyControls = function(e) {
  const elemId = e.target.id;
  switch (elemId) {
    case "flexGrowControl": {
      growControl.value = flexGrowControl.value;
      growControl.dispatchEvent(new Event("change"));
      break;
    }
    case "flexShrinkControl": {
      shrinkControl.value = flexShrinkControl.value;
      shrinkControl.dispatchEvent(new Event("change"));
      break;
    }
    case "flexBasisControl": {
      basisControl.value = flexBasisControl.value;
      basisControl.dispatchEvent(new Event("change"));
      break;
    }
    default:
      break;
  }
};

const flexPropertyControls = [
  flexGrowControl,
  flexShrinkControl,
  flexBasisControl
].forEach(elem => elem.addEventListener("change", setFlexPropertyControls));
